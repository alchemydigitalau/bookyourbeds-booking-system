<?php 
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['iso_currency'] = array(
	'GBP'	=> array(
					'name'		=> 'Pound Sterling',
					'marker'	=> array(
										'symbol' => '&#163;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'SagePay_Form', 'PayPal')
					),
	'USD'	=> array(
					'name'		=> 'U.S. Dollar',
					'marker'	=> array(
										'symbol' => '&#36;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'EUR'	=> array(
					'name'		=> 'Euro',
					'marker'	=> array(
										'symbol' => '&#8364;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'SagePay_Form', 'PayPal')
					),
	'CAD'	=> array(
					'name'		=> 'Canadian Dollar',
					'marker'	=> array(
										'symbol' => '&#36;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'AUD'	=> array(
					'name'		=> 'Australian Dollar',
					'marker'	=> array(
										'symbol' => '&#36;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'NZD'	=> array(
					'name'		=> 'New Zealand Dollar',
					'marker'	=> array(
										'symbol' => '&#36;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'CZK'	=> array(
					'name'		=> 'Czech Koruna',
					'marker'	=> array(
										'symbol' => 'K&#269;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'DKK'	=> array(
					'name'		=> 'Danish Krone',
					'marker'	=> array(
										'symbol' => 'kr',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),

	'HKD'	=> array(
					'name'		=> 'Hong Kong Dollar',
					'marker'	=> array(
										'symbol' => '&#36;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'JPY'	=> array(
					'name'		=> 'Japanese Yen',
					'marker'	=> array(
										'symbol' => '&#x00A5;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'NOK'	=> array(
					'name'		=> 'Norwegian Krone',
					'marker'	=> array(
										'symbol' => 'kr',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'PLN'	=> array(
					'name'		=> 'Polish Zloty',
					'marker'	=> array(
										'symbol' => 'z&#322;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'SGD'	=> array(
					'name'		=> 'Singapore Dollar',
					'marker'	=> array(
										'symbol' => '&#36;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'SEK'	=> array(
					'name'		=> 'Swedish Krona',
					'marker'	=> array(
										'symbol' => 'kr',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'CHF'	=> array(
					'name'		=> 'Swiss Franc',
					'marker'	=> array(
										'symbol' => 'CHF',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'THB'	=> array(
					'name'		=> 'Thai Baht',
					'marker'	=> array(
										'symbol' => '&#x0E3F;',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),
	'MYR'	=> array(
					'name'		=> 'Malaysian Ringgit',
					'marker'	=> array(
										'symbol' => 'RM',
										'position'	=> 'before'
										),
					'gateways'	=> array('NoGateway', 'PayPal')
					),

	);