<?php  
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('currency_symbol'))
{
	function currency_symbol($iso_code = null)
	{
		if(empty($iso_code))
		{
			$iso_code = account('currency');
		}

		$ci =& get_instance();

		$ci->config->load('currency');

		$c = $ci->config->item('iso_currency');

		return $c[$iso_code]['marker']['symbol'];

	}
}

if ( ! function_exists('as_currency'))
{
	function as_currency($n, $full = TRUE, $return_zero = TRUE, $prefix = TRUE)
	{

		if ( ! empty($n))
		{
			$number = number_format($n, 2, '.', '');
			
			$return = ($full) ? number_format($number, 2, '.', '') : $number;
		} else
		{
			$return = ($return_zero) ? '0.00' : null;
		}

		if(is_null($return))
		{
			return null;
		}

		if($prefix)
		{
			$ci =& get_instance();

			$ci->config->load('currency');

			$c = $ci->config->item('iso_currency');

			if($c[account('currency')]['marker']['position'] == 'before')
			{
				return $c[account('currency')]['marker']['symbol'] . $return;
			} else
			{
				return $return . $c[account('currency')]['marker']['symbol'];
			}

		}

		return $return;
	}
}


/* End of file MY_number_helper.php */
/* Location: ./system/application/helpers/MY_number_helper.php */