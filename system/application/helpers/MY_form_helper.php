<?php  
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('country_dropdown'))
{
	function country_dropdown($name, $selected = null, $attr = null)
	{
		$ci =& get_instance();

		$ci->config->load('countries');

		return form_dropdown($name, 
								array_merge(array('0' => 'Please select...'), $ci->config->item('iso_countries')),
								$selected,
								$attr);
	}
}

if ( ! function_exists('us_state_dropdown'))
{
	function us_state_dropdown($name, $selected = null, $attr = null)
	{
		$ci =& get_instance();

		$ci->config->load('countries');

		return form_dropdown($name, 
								array_merge(array('0' => 'Please select...'), $ci->config->item('us_states')),
								$selected,
								$attr);
	}
}

if ( ! function_exists('payment_gateway_dropdown'))
{
	function payment_gateway_dropdown($name, $selected = null, $attr = null, $currency = null)
	{
		$ci =& get_instance();

		$ci->config->load('currency');

		$c = $ci->config->item('iso_currency');

		$g = array();

		$currency = ( ! empty($currency)) ? $currency : account('currency');

		foreach($c[$currency]['gateways'] as $gateway)
		{
			switch($gateway)
			{
				case 'NoGateway':
					$g[$gateway] = 'No payment gateway';
					break;

				case 'SagePay_Form':
					$g[$gateway] = 'SagePay FORM';
					break;

				case 'PayPal':
					$g[$gateway] = 'PayPal Web Payments Standard';
					break;
			}
		}

		return form_dropdown($name, 
								$g,
								$selected,
								$attr);
	}

}

if ( ! function_exists('currency_dropdown'))
{
	function currency_dropdown($name, $selected = null, $attr = null)
	{
		$ci =& get_instance();

		$ci->config->load('currency');

		$c = array();

		foreach($ci->config->item('iso_currency') as $key => $currency)
		{
			$c[$key] = $currency['name'];
		}

		return form_dropdown($name, 
								$c,
								$selected,
								$attr);
	}
}

