<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

class Restrictions extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->template->set_partial('settings_menu', 'admin/partials/settings_menu');
	}

	public function index()
	{
		$data['restrictions'] = $this->model('restriction')->get_all(account('id'));

		$this->template
				->build('admin/restrictions/index', $data);	
	}

	public function resource($id = null)
	{
		
		if(empty($id))
		{
			show_404();
		}

		$data['resource'] = $this->model('resource')->get($id, account('id'));
		$data['restrictions'] = $this->model('restriction')->get_resource($id);

		$this->template
				->set_partial('resource_menu', 'admin/partials/resource_menu')
				->set_partial('inactive_room_alert', 'admin/partials/inactive_room_alert')
				->build('admin/restrictions/resource', $data);	
	}

	public function create()
	{

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('restriction[restriction_label]', 'Label', 'trim');
		$this->form_validation->set_rules('restriction[restriction_minimum]', 'Minimum', 'trim|is_numeric|callback_set_to_zero');
		$this->form_validation->set_rules('restriction[restriction_maximum]', 'Maximum', 'trim|is_numeric|callback_set_to_zero|callback_minimum_or_maximum');
		$this->form_validation->set_rules('restriction[restriction_start_at]', 'Start', 'trim|required');
		$this->form_validation->set_rules('restriction[restriction_end_at]', 'End', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['resources'] = $this->model('resource')->get_all();

			$this->template
				->build('admin/restrictions/create', $data);
		} else
		{
			$this->model('restriction')->insert($this->input->post('restriction'));

			$this->session->set_flashdata('msg', 'New minimum/maximum stay succesfully created');

			redirect("admin/restrictions");
		}
		
	}

	public function minimum_or_maximum($str)
	{
		$this->form_validation->set_message('minimum_or_maximum', 'You must have either a minimum or maximum value');

		return ( ! empty($_POST['restriction']['restriction_minimum']) || ! empty($_POST['restriction']['restriction_maximum']));
	}
	
	public function edit($id = null)
	{
		if(empty($id))
		{
			show_404();
		}

		$data['restriction'] = $this->model('restriction')->get($id);

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('restriction[restriction_label]', 'Label', 'trim');
		$this->form_validation->set_rules('restriction[restriction_minimum]', 'Minimum', 'trim|is_numeric');
		$this->form_validation->set_rules('restriction[restriction_maximum]', 'Maximum', 'trim|is_numeric|callback_minimum_or_maximum');
		$this->form_validation->set_rules('restriction[restriction_start_at]', 'Start', 'trim|required');
		$this->form_validation->set_rules('restriction[restriction_end_at]', 'End', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['resources'] = $this->model('resource')->get_all();

			$this->template
				->build('admin/restrictions/edit', $data);
		} else
		{
			$this->model('restriction')->update($this->input->post('restriction_id'), $this->input->post('restriction'));

			$this->session->set_flashdata('msg', 'Minimum/maximum stay restriction updated');

			redirect("admin/restrictions/edit/" . $this->input->post('restriction_id'));
		}
		
	}

	public function delete($id)
	{
		$this->model('restriction')->remove($id, account('id'));

		redirect("admin/restrictions");
	}
 
}