<?php 
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Internal_Controller {

	public function index()
	{
		if(session('internal_user', 'internal_user_is_agent'))
		{
			redirect(site_url('internal/accounts'));
		}

		$data['users'] = $this->model('internal_user')->get_all();

		$this->template->build('internal/users/index', $data);
	}

	public function referrals($user_id)
	{
		$data['user'] = $this->model('internal_user')->get($user_id);
		$data['referrals'] = $this->model('internal_user')->get_referrals($user_id);

		$this->template
					->build('internal/users/referrals', $data);
		
	}

	public function create()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user[internal_user_firstname]', 'First Name', 'trim');
		$this->form_validation->set_rules('user[internal_user_lastname]', 'Last Name', 'trim');
		$this->form_validation->set_rules('user[internal_user_username]', 'Username', 'trim|callback_check_username');
		$this->form_validation->set_rules('user[internal_user_email]', 'Email Address', 'trim|valid_email');
		$this->form_validation->set_rules('user[internal_user_is_agent]', 'Agent', 'trim|callback_set_to_zero');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');

		if($this->form_validation->run() === FALSE)
		{
			$this->template->build('internal/users/create');
		} else
		{
			$user = $this->input->post('user');

			$this->load->library('PasswordHash', array(8, FALSE));

			$user['internal_user_password'] = $this->passwordhash->HashPassword($this->input->post('password'));

			$id = $this->model('internal_user')->insert($user);

			$this->model('referral')->insert(array('referral_internal_user_id' => $id));

			$this->session->set_flashdata('msg', 'User created');

			redirect(site_url('internal/users/'));
		}
	}

	public function me()
	{
		$data['user'] = $this->model('internal_user')->get(session('internal_user', 'internal_user_id'));

		$this->template->build('internal/users/edit', $data);
	}

	public function edit($id)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user[internal_user_firstname]', 'First Name', 'trim');
		$this->form_validation->set_rules('user[internal_user_lastname]', 'Last Name', 'trim');
		$this->form_validation->set_rules('user[internal_user_username]', 'Username', 'trim|callback_check_username');
		$this->form_validation->set_rules('user[internal_user_email]', 'Email Address', 'trim|valid_email');

		if( ! empty($_POST['password']))
		{
			$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
		}

		if( ! session('internal_user', 'internal_user_is_agent'))
		{
			$this->form_validation->set_rules('user[internal_user_is_agent]', 'Agent', 'trim|callback_set_to_zero');
		}

		if($this->form_validation->run() === FALSE)
		{
			$data['user'] = $this->model('internal_user')->get($id);

			$this->template
					->build('internal/users/edit', $data);
		} else
		{
			$user = $this->input->post('user');

			if($this->input->post('password'))
			{
				$this->load->library('PasswordHash', array(8, FALSE));

				$user['internal_user_password'] = $this->passwordhash->HashPassword($this->input->post('password'));
			}

			$this->model('internal_user')->update($this->input->post('user_id'), $user);

			$this->session->set_flashdata('msg', 'User updated');

			redirect(site_url('internal/users/edit/' . $this->input->post('user_id')));
		}
	}

	public function check_username($str)
	{
		$this->form_validation->set_message('check_username', 'That Username has already been taken.');
		return (empty($str)) ? TRUE : $this->model('internal_user')->check_unique('username', $str, $this->input->post('user_id'));
	}

	public function remove($id)
	{
		if( ! session('internal_user', 'internal_user_is_agent') && session('internal_user', 'internal_user_id') != $id)
		{
			$this->model('internal_user')->delete($id);
			$this->session->set_flashdata('msg', 'User removed');
		}

		redirect(site_url('internal/users'));
	}


}