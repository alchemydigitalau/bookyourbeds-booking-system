<h1 class="page-header">Settings</h1>

<?php echo $template['partials']['settings_menu']; ?>

<h2>Referrals</h2>

<form class="form-horizontal">
<div class="control-group">
	<label class="control-label">Your Referral Code:</label>
	<div class="controls">
		<code style="font-size: 120%"><?php echo $account->referral_code; ?></code>
	</div>
</div>
</form>
