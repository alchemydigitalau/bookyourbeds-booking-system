<?php 
$this->config->load('currency');
$c = $this->config->item('iso_currency');

$currency = $c[account('currency')];


if( ! empty($resources))
{
	foreach($resources as $resource) {
		$this->load->view('admin/partials/availability_row', array('resource' => $resource, 'currency' => $currency));
	}
} else
{
	if( ! empty($resource))
	{
		$this->load->view('admin/partials/availability_row', array('resource' => $resource, 'currency' => $currency, 'hide_title' => TRUE));
	} else
	{
		
	}
}
?>