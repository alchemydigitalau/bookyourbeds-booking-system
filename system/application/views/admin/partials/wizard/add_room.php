<li class="well well-small media" id="add_room">
	<a class="pull-left" href="#">
		<img src="/assets/img/wizard/room-icon.png" />
	</a>
	
	<div class="media-body">
		<h4 class="media-heading">Add <?php echo ($this->account->rooms == 0) ? 'your first' : 'another' ;?> room</h4>

		<p>Your must create at least one room to enable you to launch your site, however you are not required to enter all room information at this time. You can enter additional rooms later.</p>

		<a href="#" onclick="$('#room_form').slideToggle(); return false;">Click here...</a>

		<?php echo form_open('admin/dashboard/wizard', 'class="form-horizontal' . ((empty($_add_room_open)) ? ' hide' : '') . '" id="room_form"', array('_form' => 'add_room', 'resource[resource_account_id]' => account('id'))); ?>
			<?php echo $template['partials']['form_errors']; ?>

			<fieldset>
				<legend>About the Room</legend>

				<div class="control-group">
					<label class="control-label" for="resource_title">Room Name</label>
					<div class="controls">
						<?php
						echo form_input(array(
							'name'	=> 'resource[resource_title]',
							'id'	=> 'resource_title',
							'class'	=> 'span4',
							'value'	=> set_value('resource[resource_title]')
							));
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="resource_priced_per">Priced per</label>
					<div class="controls">
						<?php
						echo form_dropdown('resource[resource_priced_per]', 
											array('room' => 'room', 'bed' => 'bed'), 
											set_value('resource[resource_priced_per]', 'room'),
											'class="span2"');	
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="resource_booking_footprint">Occupancy</label>
					<div class="controls">
						<?php
						echo form_input(array(
							'name'	=> 'resource[resource_booking_footprint]',
							'id'	=> 'resource_booking_footprint',
							'class'	=> 'span1',
							'value'	=> set_value('resource[resource_booking_footprint]')
							));
						?>
						<span class="help-block">This is the number of guests each one of these resources can accommodate.</span>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="resource_default_release">Default Release</label>
					<div class="controls">
						<?php
						echo form_input(array(
							'name'	=> 'resource[resource_default_release]',
							'id'	=> 'resource_default_release',
							'class'	=> 'span1',
							'value'	=> set_value('resource[resource_default_release]')
							));
						?>
						<span class="help-block">This is the number of rooms/beds of this type, not the number of guests you can accommodate.</span>
					</div>
				</div>

			</fieldset>

			<fieldset>
				<legend>Pricing</legend>

				
						<table class="table">
							<thead>
								<tr>
									<th>Monday</th>
									<th>Tuesday</th>
									<th>Wednesday</th>
									<th>Thursday</th>
									<th class="weekend">Friday</th>
									<th class="weekend">Saturday</th>
									<th>Sunday</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
									<?php
									$this->config->load('currency');
									$c = $this->config->item('iso_currency');

									$currency = $c[account('currency')];



									?>



										<div class="<?php echo ($currency['marker']['position'] == 'before') ? 'input-prepend' : 'input-append'; ?>">
											<?php if($currency['marker']['position'] == 'before') { ?>
  											<span class="add-on"><?php echo $currency['marker']['symbol']; ?></span>
  											<?php } ?>
  											
  											<?php
											$input = array(
												'name'	=> 'price[1]',
												'class'	=> 'span1',
												'value'	=> set_value('price[1]')
											);
											
											echo form_input($input);
											
											?>

											<?php if($currency['marker']['position'] == 'after') { ?>
  											<span class="add-on"><?php echo $currency['marker']['symbol']; ?></span>
  											<?php } ?>
										</div>
										
										<!--<a href="#" onclick="$('.season_0').val($('.season_0.first').val()); return false;" class="link_fields" title="Link">j</a>-->
									</td>
									<?php for($i = 2; $i <= 7; $i++) { ?>
									<td<?php echo ($i == 5 || $i == 6) ? ' class="weekend"' : ''; ?>>
										<div class="<?php echo ($currency['marker']['position'] == 'before') ? 'input-prepend' : 'input-append'; ?>">
  											<?php if($currency['marker']['position'] == 'before') { ?>
  											<span class="add-on"><?php echo $currency['marker']['symbol']; ?></span>
  											<?php } ?>
											
											<?php
											$input = array(
												'name'	=> "price[{$i}]",
												'class'	=> 'span1',
												'value'	=> set_value("price[{$i}]")
											);
											
											echo form_input($input);
											?>

											<?php if($currency['marker']['position'] == 'after') { ?>
  											<span class="add-on"><?php echo $currency['marker']['symbol']; ?></span>
  											<?php } ?>
										</div>
									</td>
									<?php } ?>
									
								</tr>
							</tbody>
						</table>

			</fieldset>

			<fieldset>
				<legend>Sales Tax (Optional)</legend>

				<div class="control-group">
					<label class="control-label" for="resource_tax_amount">Sales tax on this room</label>
					<div class="controls">
						<div class="input-append">
						<?php
						echo form_input(array(
							'name'	=> 'resource[resource_tax_amount]',
							'id'	=> 'resource_tax_amount',
							'class'	=> 'span1',
							'value'	=> set_value('resource[resource_tax_amount]')));
						?>

						<span class="add-on">%</span>
						</div>

						<span class="help-block">If you enter a sales tax amount your booking confirmations will include tax calculations.</span>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<label>
						<?php
						echo form_checkbox(array(
							'name'	=> 'resource[resource_tax_inclusive]',
							'value'	=> 1,
							'checked' => set_checkbox('resource[resource_tax_inclusive]', 1, TRUE)));
						?>
						This tax is included in price figures
						</label>
					</div>
				</div>

			</fieldset>

			<div class="control-group">
				
				<div class="controls">
					<button type="submit" class="btn btn-warning btn-large">Add Room</button>
				</div>
			</div>

		</form>	
	</div>
</li>