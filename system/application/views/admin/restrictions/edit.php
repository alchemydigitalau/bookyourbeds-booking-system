<h1 class="page-header">Settings</h1>

<?php echo $template['partials']['settings_menu']; ?>

<h2>Edit Stay Restriction</h2>

<?php echo validation_errors(); ?>

<?php echo form_open("admin/restrictions/edit/" . $restriction->restriction_id, 'class="form-horizontal"', array('restriction_id' => $restriction->restriction_id)); ?>
	<fieldset>
		<div class="control-group">
			<label class="control-label" for="restriction_label">Label</label>
			<div class="controls">
				<?php
				echo form_input(array(
					'name'	=> 'restriction[restriction_label]',
					'id'	=> 'restriction_label',
					'class'	=> 'xlarge',
					'value'	=> set_value('restriction[restriction_label]', $restriction->restriction_label)
					));
				?>
			</div>
		</div> <!-- /clearfix -->

		<div class="control-group">
			<label class="control-label" for="restriction_minimum">Minimum Nights Stay</label>
			<div class="controls">
				<?php
				echo form_input(array(
					'name'	=> 'restriction[restriction_minimum]',
					'id'	=> 'restriction_minimum',
					'class'	=> 'span1',
					'value'	=> set_value('restriction[restriction_minimum]', $restriction->restriction_minimum)
					));
				?>
				<span class="help-block">Leave blank for no minimum</span>
			</div>
		</div> <!-- /clearfix -->

		<div class="control-group">
			<label class="control-label" for="restriction_minimum">Maximum Nights Stay</label>
			<div class="controls">
				<?php
				echo form_input(array(
					'name'	=> 'restriction[restriction_maximum]',
					'id'	=> 'restriction_maximum',
					'class'	=> 'span1',
					'value'	=> set_value('restriction[restriction_maximum]', $restriction->restriction_maximum)
					));
				?>
				<span class="help-block">Leave blank for no maximum</span>
			</div>
		</div> <!-- /clearfix -->

		<div class="control-group">
			<label class="control-label" for="start_datepicker">Effective Period Starts</label>
			<div class="controls">
				<?php
				echo form_input(array(
					'name'	=> 'restriction[restriction_start_at]',
					'id'	=> 'start_datepicker',
					'class'	=> 'medium',
					'value'	=> set_value('restriction[restriction_start_at]', mysql_to_format($restriction->restriction_start_at, 'd/m/Y'))
					));
				?>
			</div>
		</div> <!-- /clearfix -->

		<div class="control-group">
			<label class="control-label" for="end_datepicker">Effective Period Ends</label>
			<div class="controls">
				<?php
				echo form_input(array(
					'name'	=> 'restriction[restriction_end_at]',
					'id'	=> 'end_datepicker',
					'class'	=> 'medium',
					'value'	=> set_value('restriction[restriction_end_at]', mysql_to_format($restriction->restriction_end_at, 'd/m/Y'))
					));
				?>
			</div>
		</div> <!-- /clearfix -->

		<div class="control-group">
			<label class="control-label">Room Types</label>
			<div class="controls">

				<?php
				foreach($resources as $resource)
				{
					echo '<label>';

					echo form_checkbox(array(
										'name'	=> "restriction[resources][{$resource->resource_id}]",
										'value'	=> $resource->resource_id,
										'checked' => in_array($resource->resource_id, $restriction->resources)
										));

					echo ' ' . $resource->resource_title;


					echo '</label>';
				}

				
				?>
			</div>
		</div> <!-- /clearfix -->

		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary">Save Changes</button>

				<a href="<?php echo site_url('admin/restrictions/delete/' . $restriction->restriction_id); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to permanently delete this stay restriction?');">Delete this restriction</a>
			</div>
		</div>
	</fieldset>

</form>

<!-- start: page-specific javascript -->
<script type="text/javascript">
<!--
	$(function(){
			
		$('#start_datepicker').datepicker({
			changeMonth: true,
			changeYear: true,
			firstDay: 1,
			dateFormat: 'dd/mm/yy',
			showAnim: 'fadeIn',
			showOn: 'button',
			buttonText: '<i class="icon-calendar"></i>',
			onClose: 
			function(dateText, inst) 
			{ 
				var d = dateText.split('/'); 
				var ts = $.datepicker.formatDate('@', new Date(d[2], d[1] - 1, d[0]));
				var ds = 60 * 60 * 24 * 1000;
				
				var min_d = new Date();
				min_d.setTime(Number(ts) + (Number(ds) * 1));
				
				$('#end_datepicker').datepicker('option', 'minDate', min_d);
			}
		});
		
		$('#end_datepicker').datepicker({
			changeMonth: true,
			changeYear: true,
			firstDay: 1,
			dateFormat: 'dd/mm/yy',
			showAnim: 'fadeIn',
			showOn: 'button',
			buttonText: '<i class="icon-calendar"></i>'
		});
		
		
	});
	

-->
</script>
<!-- end: page-specific javascript -->