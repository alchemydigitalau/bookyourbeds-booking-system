<?php echo $template['partials']['inactive_room_alert']; ?>

<h1 class="page-header">Edit Room <small><?php echo $resource->resource_title; ?></small></h1>

<?php echo $template['partials']['resource_menu']; ?>

<h2>Stay Restrictions</h2>

<p><?php echo anchor('admin/restrictions/create', 'Create New Restriction', 'class="btn primary plus"'); ?></p>

<?php if (! empty($restrictions)) { ?>
<table class="table table-condensed table-striped table-hover">
	<thead>
		<tr>
			<th>Label</th>
			<th>Start</th>
			<th>End</th>
			<th>Min. Nights</th>
			<th>Max. Nights</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
		<?php foreach($restrictions as $restriction) { ?>
		<tr>
			<td><?php echo $restriction->restriction_label; ?></td>
			<td><?php echo mysql_to_format($restriction->restriction_start_at); ?></td>
			<td><?php echo mysql_to_format($restriction->restriction_end_at); ?></td>
			<td><?php echo ( ! empty($restriction->restriction_minimum)) ? $restriction->restriction_minimum : 'N/A'; ?></td>
			<td><?php echo ( ! empty($restriction->restriction_maximum)) ? $restriction->restriction_maximum : 'N/A'; ?></td>
			<td><?php echo anchor('admin/restrictions/edit/' . $restriction->restriction_id, 'Edit'); ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php } else { ?>
<div class="alert alert-info">You have no restrictions set up. <?php echo anchor('admin/restrictions/create', 'Create one...'); ?></div>
<?php } ?>