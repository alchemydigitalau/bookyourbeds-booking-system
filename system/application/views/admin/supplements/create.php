<div class="page-header row">
	<h1>Create Supplement</h1>
</div>


		<?php echo $template['partials']['form_errors']; ?>

		<?php echo form_open('admin/supplements/create', 'class="form-horizontal"', array('supplement[supplement_account_id]' => account('id'))); ?>
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="supplement_short_description">Short Description/Name</label>
					<div class="controls">
						<?php
						echo form_input(array(
							'name'	=> 'supplement[supplement_short_description]',
							'id'	=> 'resource_title',
							'class'	=> 'span4',
							'value'	=> set_value('supplement[supplement_short_description]')
							));
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">Long Description</label>
					<div class="controls">
						<?php echo form_textarea(array(
											'name'	=> 'supplement[supplement_long_description]',
											'class'	=> 'span4',
											'rows'	=> 4,
											'value'	=> set_value('supplement[supplement_long_description]')
											));
						?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="resource_default_release">Default Price</label>
					<div class="controls">
						<?php
						$this->config->load('currency');
						$c = $this->config->item('iso_currency');

						$currency = $c[account('currency')];
						?>
						<div class="<?php echo ($currency['marker']['position'] == 'before') ? 'input-prepend' : 'input-append'; ?>">
							<?php if($currency['marker']['position'] == 'before') { ?>
							<span class="add-on"><?php echo $currency['marker']['symbol']; ?></span>
							<?php } 

							echo form_input(array(
								'name'	=> 'supplement[supplement_default_price]',
								'class'	=> 'span1',
								'value'	=> set_value('supplement[supplement_default_price]')
								));
							
							if($currency['marker']['position'] == 'after') { ?>
							<span class="add-on"><?php echo $currency['marker']['symbol']; ?></span>
							<?php } ?>
						</div>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="resource_default_release">Priced Per</label>
					<div class="controls">
						<?php
						echo form_dropdown('supplement[supplement_per_guest]', 
											array('0' => 'room', '1' => 'guest'), 
											set_value('supplement[supplement_per_guest]'),
											'class="span2"');	
						?>
						&nbsp;per&nbsp;
						<?php
						echo form_dropdown('supplement[supplement_per_day]', 
											array('0' => 'stay', '1' => 'day'), 
											set_value('supplement[supplement_per_day]'),
											'class="span1"');	
						?>
					</div>
				</div>
			</fieldset>

			<?php if(account('tax_calculations')) { ?>
			<fieldset>
				<legend>Sales Tax</legend>

				<div class="control-group">
					<label class="control-label" for="supplement_tax_amount">Sales tax on this supplement</label>
					<div class="controls">
						<div class="input-append">
						<?php
						echo form_input(array(
							'name'	=> 'supplement[supplement_tax_amount]',
							'id'	=> 'supplement_tax_amount',
							'class'	=> 'span1',
							'value'	=> set_value('supplement[supplement_tax_amount]')));
						?>

						<span class="add-on">%</span>
						</div>

						<span class="help-block">If you enter a sales tax amount your booking confirmations will include tax calculations.</span>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<label>
						<?php
						echo form_checkbox(array(
							'name'	=> 'supplement[supplement_tax_inclusive]',
							'value'	=> 1,
							'checked' => set_checkbox('supplemente[supplement_tax_inclusive]', 1, TRUE)));
						?>
						This tax is included in price figures
						</label>
					</div>
				</div>

			</fieldset>
			<?php } ?>


			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-warning btn-large">Save Changes</button>
				</div>
			</div>
		</form>

