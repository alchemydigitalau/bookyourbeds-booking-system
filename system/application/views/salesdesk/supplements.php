<?php echo $template['partials']['cart']; 

?>

	<h1 class="page-header">Optional Supplements</h1>

	<?php echo $template['partials']['form_errors']; ?>

	<?php echo form_open('salesdesk/supplements', array('class' => 'form-horizontal')); ?>

	<?php 
	$_supplements = booking('supplements');

	foreach($supplements as $rid => $resource) { ?>
		<h3><?php echo $resource->resource_title; ?></h3>

		<table class="table">
			<tbody>
			<?php foreach($resource->supplements as $supplement) { ?>
				<?php
				if(account('tax_calculations') && $supplement->resource_price > 0)
				{
					if($supplement->supplement_tax_inclusive)
					{
						$supplement_price = $supplement->resource_price;
					} else
					{
						$tax = ($supplement->supplement_tax_amount > 0) ? ($supplement->resource_price / $supplement->supplement_tax_amount) : 0;
						$supplement_price = $supplement->resource_price + $tax;
					}

					$tax_msg = '(inc. sales tax) ';
				} else
				{
					$tax_msg = '';
					$supplement_price = $supplement->resource_price;
				}
				?>

				<tr>
					<td><h4><?php echo $supplement->supplement_short_description; ?> <small><?php echo as_currency($supplement_price) . ' ' . $tax_msg .
																								(($supplement->supplement_per_guest) ? 'per person' : 'per ' . $resource->resource_priced_per) . ' ' .
																								(($supplement->supplement_per_day) ? 'per night' : 'per stay') ; ?></small></h4>

						<?php echo auto_typography($supplement->supplement_long_description); ?>
					</td>

					<td class="span2">
						<?php
						// The total number of options
						$opt_count = ($supplement->supplement_per_guest) ? $booking->resources[$rid]->reservation_guests : $booking->resources[$rid]->reservation_footprint;
						$multiply = ($supplement->supplement_per_day) ? $booking->resources[$rid]->reservation_duration : 1;

						$options = array(
										0	=> '0'
										);

						for($i = 1; $i <= $opt_count; $i++)
						{
							$options[$i] = $i . ' ' . (($supplement->supplement_per_guest) ? 'person' : $resource->resource_priced_per) . (($i > 1) ? 's' : '') . ' - ' . as_currency($supplement_price * $multiply * $i);
						}

						echo form_dropdown("supplements[{$resource->resource_id}][{$supplement->supplement_id}][qty]", 
											$options, 
											set_value("supplements[{$resource->resource_id}][{$supplement->supplement_id}][qty]", ( ! empty($_supplements[$resource->resource_id][$supplement->supplement_id])) ? $_supplements[$resource->resource_id][$supplement->supplement_id]['qty'] : 0), 
											'class="span2"');
						
						echo form_hidden(array(
											"supplements[{$resource->resource_id}][{$supplement->supplement_id}][price]" => ($supplement_price * $multiply),
											"supplements[{$resource->resource_id}][{$supplement->supplement_id}][description]" => $supplement->supplement_short_description . ' (' . $resource->resource_title . ')'
											));

						if(account('tax_calculations')) { 
							if($supplement->supplement_tax_inclusive)
							{
								$tax = $supplement_price - ($supplement->resource_price * (100 / (100 + $supplement->supplement_tax_amount)));
							} else
							{
								$tax = ($supplement->supplement_tax_amount > 0) ? ($supplement->resource_price / $supplement->supplement_tax_amount) : 0;
							}

							echo form_hidden(
											"supplements[{$resource->resource_id}][{$supplement->supplement_id}][tax]", ($tax)
											);
						}
						?>

					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>

	<?php } ?>



	
		
	<div class="control-group">

		<div class="controls">
			<button type="submit" class="btn btn-primary">Continue</button>
		</div>
	</div>

	</form>

