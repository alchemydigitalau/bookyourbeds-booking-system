<?php echo form_open('salesdesk/new_booking', 
					array(
						'method' => 'POST',
						'onsubmit' => 'return checkGuests(' . $guests . ');',
						'class'		=> 'form-horizontal'
						), 
					array(
						'guests' 			=> $guests,
						'start_timestamp'	=> $start_timestamp,
						'duration'			=> $duration
						)); ?>



<?php 

foreach($resources as $resource) { 
			
$availability =& $resource->availability; ?>



<div>
	<h4><?php echo $resource->resource_title; ?></h4>

	<table class="table table-condensed table-striped table-bordered">
	
	<thead>
		<tr>
			<?php for($i = 0; $i < $duration; $i++) { ?>
			<th class="align_center span2<?php echo (date("w", strtotime('+' . $i . ' day', $start_timestamp)) > 4 ) ? ' weekend' : ''; ?>">
			<?php
			echo date("D", strtotime('+' . $i . ' day', $start_timestamp)); ?><br /><small><?php echo date("d/m", strtotime('+' . $i . ' day', $start_timestamp)); ?></small>
			</th>
			<?php } ?>
			<th class="span2"></th>
		</tr>
	</thead>

	<tbody id="availability_results">
		<tr id="row_<?php echo $resource->resource_id; ?>">
			
			<?php 
			$available = TRUE;
			$msg = '<span class="label label-important">No Availability</span>';
			$max = $guests;
			$single_total = 0;

			if( ! empty($availability['restriction']))
			{

				if($duration < $availability['restriction']->restriction_minimum)
				{
					$overlap = $duration;
				} else if($start_timestamp <= human_to_unix($availability['restriction']->restriction_start_at))
				{
					$overlap = ceil((strtotime("+{$duration} days", $start_timestamp) - human_to_unix($availability['restriction']->restriction_start_at)) / (60 * 60 * 24));
				} else
				{
					$overlap = ceil((human_to_unix($availability['restriction']->restriction_end_at) - $start_timestamp) / (60 * 60 * 24));
				}

				if( ! empty($availability['restriction']->restriction_minimum) && $overlap < $availability['restriction']->restriction_minimum)
				{
					$available = FALSE;
					$msg = '<div class="label label-important">';
					$msg .= 'Minimum of ' . $availability['restriction']->restriction_minimum . ' nights<br />consecutive stay between<br />';
					$msg .= mysql_to_format($availability['restriction']->restriction_start_at) . ' &amp; ';
					$msg .= mysql_to_format($availability['restriction']->restriction_end_at);
					$msg .= '</div>';
				} else if ( ! empty($availability['restriction']->restriction_maximum) && $overlap > $availability['restriction']->restriction_maximum)
				{
					$available = FALSE;
					$msg = '<div class="label label-important">';
					$msg .= 'Maximum of ' . $availability['restriction']->restriction_maximum . ' nights<br />consecutive stay between<br />';
					$msg .= mysql_to_format($availability['restriction']->restriction_start_at) . ' &amp; ';
					$msg .= mysql_to_format($availability['restriction']->restriction_end_at);
					$msg .= '</div>';
				}
			}

			for($i = 1; $i <= $duration; $i++) { ?>

			<td class="align_center">
				
				<?php
				$b = ( ! empty($availability['dates'][$i]->bookings_pending)) ? $availability['dates'][$i]->bookings + $availability['dates'][$i]->bookings_pending + $availability['dates'][$i]->bookings_unverified  : $availability['dates'][$i]->bookings + $availability['dates'][$i]->bookings_unverified;
				
				$a = ($availability['dates'][$i]->release - $b) * $resource->resource_booking_footprint;

				$e = $availability['dates'][$i]->release - $b;

				if($e > 0)
				{
					

					echo '<i class="icon-ok"></i><br /><small>';

					echo $e . ' available<br />';

					$single_total += $availability['dates'][$i]->price;			

					echo as_currency($availability['dates'][$i]->price) . '/' . $resource->resource_priced_per;

					if(account('tax_calculations'))
					{
						echo ($resource->resource_tax_inclusive) ? '<br />inc. sales tax' : '<br />exc. sales tax';
					}

					echo '</small>';
					
					$max = ($a < $max) ? $a : $max;
				} else
				{
					echo '<i class="icon-remove"></i><br />&nbsp;';
					$available = FALSE;
					$max = 0;
				}
				?>		
			</td>
			<?php } ?>
			
			<td class="align_center">
				<?php if($available) { ?>
				
				<?php
				$dropdown = array('0' => 'Select...');

				for($d = 1; $d <= $max; $d++)
				{
					$guest_plural = ($d > 1) ? 's' : '';
					$fp = ceil($d / $resource->resource_booking_footprint);
					$per_plural = ($fp > 1) ? 's' : '';

					$dropdown[$d] = $d . ' guest' . $guest_plural . ' (' . $fp . ' ' . $resource->resource_priced_per . $per_plural . ')';
				}

				$js = 'class="span2" onchange="updateRow(' . $resource->resource_id . ');"';

				echo form_dropdown("resource[{$resource->resource_id}][guests]", $dropdown, 0, $js);
				
				echo form_hidden("resource[{$resource->resource_id}][resource_title]", $resource->resource_title);
				
				echo form_hidden("resource[{$resource->resource_id}][resource_booking_footprint]", $resource->resource_booking_footprint);
				echo form_hidden("resource[{$resource->resource_id}][resource_priced_per]", $resource->resource_priced_per);


				if(account('tax_calculations'))
				{
					if($resource->resource_tax_inclusive)
					{
						$tax = $single_total - ($single_total * (100 / (100 + $resource->resource_tax_amount)));
						echo form_hidden("resource[{$resource->resource_id}][resource_single_taxable_price]", $single_total - $tax);
						echo form_hidden("resource[{$resource->resource_id}][resource_single_tax]", $tax);
						echo form_hidden("resource[{$resource->resource_id}][resource_single_price]", $single_total);
						echo form_hidden("resource[{$resource->resource_id}][resource_first_night]", $availability['dates'][1]->price);
					} else
					{
						$tax = ($resource->resource_tax_amount > 0) ? ($single_total / $resource->resource_tax_amount) : 0;
						echo form_hidden("resource[{$resource->resource_id}][resource_single_taxable_price]", $single_total);
						echo form_hidden("resource[{$resource->resource_id}][resource_single_tax]", $tax);
						echo form_hidden("resource[{$resource->resource_id}][resource_single_price]", $single_total + $tax);

						echo form_hidden("resource[{$resource->resource_id}][resource_first_night]", $availability['dates'][1]->price + ($availability['dates'][1]->price * ($resource->resource_tax_amount / 100)));
					}
					
				} else
				{
					echo form_hidden("resource[{$resource->resource_id}][resource_first_night]", $availability['dates'][1]->price);
					echo form_hidden("resource[{$resource->resource_id}][resource_single_price]", $single_total);
				}

				} else { 
				echo $msg;
				} ?>
			</td>
		</tr>
	
	</tbody>
</table>

</div>
<?php } ?>

<div id="booking_total" style="display: none;">
	<h2 class="page-header">Your Booking</h2>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Room Type</th>
				<th>Quantity</th>
				<th>Guests</th>
				<?php if(account('tax_calculations')) { ?>
				<th>Room Cost</th>
				<th>Sales Tax</th>
				<?php } ?>
				<th>Total</th>
			</tr>
		</thead>
	
		<tbody id="booking_breakdown">
		
		</tbody>
	</table>

	<div id="overbooking_warning" class="alert alert-danger" style="display: none;">
		For bookings of more than <strong><?php echo setting('max_guests_public'); ?> guests</strong> please contact <?php echo account('name'); ?> directly.
	</div>

	<div class="control-group">
		
		<div class="controls">
			<button type="submit" class="btn btn-primary btn-large" id="submit_btn" style="display: none;">BOOK NOW</button>

		</div>
	</div> 

</div>

</form>

<!-- Page specific Javascript -->
<script type="text/javascript">
<!--
	var guest_array = new Array();
	var price_array = new Array();
	<?php if(account('tax_calculations')) { ?>
	var taxable_array = new Array();
	var tax_array = new Array();
	<?php } ?>

	var total_guests = 0;
	var total_price = 0;

	function updateRow(id)
	{
		// Let's get some values...
		var guests = $('select[name="resource[' + id + '][guests]"]').val();

		if(guests == 0)
		{
			$('#total_row_' + id).remove();
			guest_array[id] = price_array[id] = 0;
		} else {
			var title = $('input[name="resource[' + id + '][resource_title]"]').val();
			<?php if(account('tax_calculations')) { ?>
			var taxable_total = $('input[name="resource[' + id + '][resource_single_taxable_price]"]').val();
			var tax = $('input[name="resource[' + id + '][resource_single_tax]"]').val();
			<?php } ?>
			var single_total = $('input[name="resource[' + id + '][resource_single_price]"]').val();
			var footprint = $('input[name="resource[' + id + '][resource_booking_footprint]"]').val();
			var priced_per = $('input[name="resource[' + id + '][resource_priced_per]"]').val();

			// These can probably be done on the fly...
			var resources = Math.ceil(guests / footprint);
			<?php if(account('tax_calculations')) { ?>
			var resource_taxable = resources * taxable_total;
			var resource_tax = resources * tax;
			<?php } ?>
			var resource_total = resources * single_total;


			//alert(resource_total);
			guest_array[id] = guests;
			<?php if(account('tax_calculations')) { ?>
			taxable_array[id] = resource_taxable;
			tax_array[id] = resource_tax;
			<?php } ?>
			price_array[id] = resource_total;


			var table_row = '<tr id="total_row_' + id + '">';
			table_row += '<td>' + title + '</td>';
			table_row += '<td>' + resources + '</td>';
			table_row += '<td>' + guests + '</td>';
			<?php if(account('tax_calculations')) { ?>
			table_row += '<td><?php echo currency_symbol(); ?>' + resource_taxable.toFixed(2) + '</td>';
			table_row += '<td><?php echo currency_symbol(); ?>' + resource_tax.toFixed(2) + '</td>';
			<?php } ?>
			table_row += '<td><?php echo currency_symbol(); ?>' + resource_total.toFixed(2) + '</td>';
			table_row += '</tr>';

			if($('#total_row_' + id).length == 0)
			{
				$('#booking_breakdown').prepend(table_row);
			} else
			{
				$('#total_row_' + id).replaceWith(table_row);
			}

			$('#booking_total').show();

			// Scroll down...
			$('html, body').animate({
		         scrollTop: $("#booking_total").offset().top
		     }, 1000);

		}

		// Add up the totals
		var index;
		total_guests = 0;
		
		for (index in guest_array) {
		    total_guests += Number(guest_array[index]);
		}

		if(total_guests > 0)
		{
			

			var index;
			total_price = 0;
			
			for (index in price_array) {
			    total_price += Number(price_array[index]);
			}

			<?php if(account('tax_calculations')) { ?>
			var index_taxable;
			total_taxable = 0;

			for (index_taxable in taxable_array) {
			    total_taxable += Number(taxable_array[index_taxable]);
			}

			var index_tax;
			total_tax = 0;

			for (index_tax in tax_array) {
			    total_tax += Number(tax_array[index_tax]);
			}
			<?php } ?>

			var table_row = '<tr id="grand_total_row">';
			table_row += '<td><strong>Grand Total</strong></td>';
			table_row += '<td></td>';
			table_row += '<td><strong>' + total_guests + '</strong></td>';
			<?php if(account('tax_calculations')) { ?>
			table_row += '<td><strong><?php echo currency_symbol(); ?>' + total_taxable.toFixed(2) + '</strong></td>';
			table_row += '<td><strong><?php echo currency_symbol(); ?>' + total_tax.toFixed(2) + '</strong></td>';
			<?php } ?>
			table_row += '<td><strong><?php echo currency_symbol(); ?>' + total_price.toFixed(2) + '</strong></td>';
			table_row += '</tr>';
		} else
		{
			var table_row = '<tr id="grand_total_row">';
			table_row += '<td colspan="4">Please select a room...</td>';
			table_row += '</tr>';
		}

		if($('#grand_total_row').length == 0)
		{
			$('#booking_breakdown').append(table_row);
		} else
		{
			$('#grand_total_row').replaceWith(table_row);
		}

		if(total_guests > 0)
		{
			if(total_guests > <?php echo setting('max_guests_public'); ?>)
			{
				$('#overbooking_warning').show();
				$('#submit_btn').hide();
			} else
			{
				$('#overbooking_warning').hide();
				$('#submit_btn').show();
			}
		} else
		{
			$('#submit_btn').hide();
		}
	}

	function checkGuests(guests)
	{
		if(guests == total_guests)
		{
			return true;
		} else
		{
			return confirm('You are making a booking for ' + total_guests + ' guests (you originally requested ' + guests + '). Is this correct?');
		}
	}
-->
</script>
<!-- // -->