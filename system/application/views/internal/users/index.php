<div class="page-header clearfix">
	<h1 class="pull-left">
		Users
	</h1>

	<div class="pull-right">
		<a href="<?php echo site_url('internal/users/create'); ?>" class="btn btn-primary btn-large">Create New User</a>
	</div>
</div>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Name</th>
			<th>Username</th>
			<th>Email Address</th>
			<th>Agent</th>
			<th>Referrals</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
	<?php foreach($users as $user) { ?>
 		<tr>
	 		<td><?php echo $user->internal_user_firstname . ' ' . $user->internal_user_lastname; ?></td>
	 		<td><?php echo $user->internal_user_username; ?></td>
	 		<td><?php echo mailto($user->internal_user_email); ?></td>
	 		<td><i class="icon icon-<?php echo ($user->internal_user_is_agent) ? 'ok' : 'remove'; ?>"></td>
	 		
			<td><?php echo anchor('internal/users/referrals/' . $user->internal_user_id, (int) $user->referrals); ?></td>
	 		<td><?php echo anchor('internal/users/edit/' . $user->internal_user_id, 'Edit'); ?></td>
 		</tr>
 	<?php } ?>
	</tbody>


</table>