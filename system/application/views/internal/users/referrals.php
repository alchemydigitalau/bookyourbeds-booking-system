<div class="page-header clearfix">
	<h1 class="pull-left">
		Account Referrals From <?php echo anchor('internal/users/edit/' . $user->internal_user_id, $user->internal_user_firstname . ' ' . $user->internal_user_lastname); ?>
	</h1>


</div>


<?php if(! empty($referrals)) { ?>
<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Account Name</th>
			<th>Contact Email</th>
			<th>Created</th>
			<th>Confirmed</th>
			<th>Activated</th>
			<th>Capacity</th>
			<th>Last Activity</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
		<?php foreach($referrals as $referral) { ?>
		<tr>
			<td><?php echo $referral->account_id; ?></td>
			<td><?php echo $referral->account_name; ?></td>
			<td><?php echo mailto($referral->account_email); ?></td>
			<td><?php echo mysql_to_format($referral->account_created_at); ?></td>
			<td><?php echo ($referral->account_confirmed) ? '<span class="label label-success">CONFIRMED</span>' : '<span class="label label-important">NOT CONFIRMED</span>'; ?></td>
			<td><?php echo ($referral->account_activated_at == '0000-00-00 00:00:00') ? '<span class="label label-important">NOT ACTIVATED</span>' : mysql_to_format($referral->account_activated_at); ?></td>
			<td><?php echo (int) $referral->account_capacity; ?></td>

			<td><?php echo (empty($referral->account_last_activity)) ? '<span class="label label-important">NEVER</span>' : nice_time($referral->account_last_activity); ?></td>

			<td><a href="<?php echo site_url('internal/accounts/edit/' . $referral->account_id); ?>" class="btn btn-primary btn-small">Edit</a></td>
		</tr>


		<?php } ?>


	</tbody>

</table>
<?php } else { ?>
<div class="alert">
	No referrals
</div>
<?php } ?>