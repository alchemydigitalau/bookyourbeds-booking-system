<h1 class="page-header">Edit User</h1>


<div class="row">
<div class="span8">
	<?php echo $template['partials']['form_errors']; ?>

	<?php echo form_open("internal/users/edit/{$user->internal_user_id}", 'class="form-horizontal"', array('user_id' => $user->internal_user_id)); ?>
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="internal_user_firstname">First Name</label>
				<div class="controls">
					<?php
					echo form_input(array(
						'name'	=> 'user[internal_user_firstname]',
						'id'	=> 'internal_user_firstname',
						'class'	=> 'span3',
						'value'	=> set_value('user[internal_user_firstname]', $user->internal_user_firstname)
						));
					?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="internal_user_lastname">Last Name</label>
				<div class="controls">
					<?php
					echo form_input(array(
						'name'	=> 'user[internal_user_lastname]',
						'id'	=> 'internal_user_lastname',
						'class'	=> 'span3',
						'value'	=> set_value('user[internal_user_lastname]', $user->internal_user_lastname)
						));
					?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="internal_user_email">Email Address</label>
				<div class="controls">
					<?php
					echo form_input(array(
						'name'	=> 'user[internal_user_email]',
						'id'	=> 'internal_user_email',
						'class'	=> 'span4',
						'value'	=> set_value('user[internal_user_email]', $user->internal_user_email)
						));
					?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="internal_user_username">Username</label>
				<div class="controls">
					<?php
					echo form_input(array(
						'name'	=> 'user[internal_user_username]',
						'id'	=> 'internal_user_username',
						'class'	=> 'span2',
						'value'	=> set_value('user[internal_user_username]', $user->internal_user_username)
						));
					?>
				</div>
			</div>

			<?php if(! session('internal_user', 'internal_user_is_agent') && session('internal_user', 'internal_user_id') != $user->internal_user_id) { ?>
			<div class="control-group">
				<div class="controls">
					<label>
					<?php
					echo form_checkbox(array(
						'name'	=> 'user[internal_user_is_agent]',
						'value'	=> 1,
						'checked' => set_checkbox('user[internal_is_agent]', 1, (bool) $user->internal_user_is_agent)
						));
					?>
					This user is a sales agent</label>
				</div>
			</div>

			<?php } ?>

	

		</fieldset>

		<fieldset>
			<legend>Change Password</legend>
			
			<div class="control-group">
		        	<label class="control-label">New Password</label>
		        	<div class="controls">
		            	<input type="password" class="span3" name="password" />
		            </div>
		        </div>

		        <div class="control-group">
		        	<label class="control-label">Confirm Password</label>
		        	<div class="controls">
		            	<input type="password" class="span3" name="passconf" />
		            </div>
		        </div>

		</fieldset>

		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>

		<?php if(! session('internal_user', 'internal_user_is_agent') && session('internal_user', 'internal_user_id') != $user->internal_user_id) { ?>
		<div class="control-group">
			<div class="controls">
				<a href="<?php echo site_url('internal/users/remove/' . $user->internal_user_id); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to remove this user?');">Remove this user</a>
			</div>
		</div>
		<?php } ?>
		
	</form>
</div>


<div class="span4">
	<div class="alert">
		<h4 class="page-header">Referrals</h4>
		<p><br />Referral Code: <code><?php echo $user->referral_code; ?></code></p>

		<p>Total Referrals: <?php echo anchor('internal/users/referrals/' . $user->internal_user_id, (int) $user->referrals . ' &raquo;'); ?></p>
	</div>
</div>
</div>