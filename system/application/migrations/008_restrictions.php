<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

class Migration_restrictions extends CI_Migration
{
	public function up()
	{
		$this->load->dbforge();

		$fields = array('account_tax_calculations'	=> array(
															'type' => 'TINYINT',
															'constraint'	=> 1,
															'default'		=> 0
															),
						'account_tax_reference' => array('type' => 'VARCHAR',
														'constraint'	=> 128,
														'null'		=> TRUE),
						'account_currency' => array('type' => 'VARCHAR',
													'constraint'	=> 3,
													'default'		=> 'GBP'),


						'account_created_by_internal_user_id' => array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        ),
						'account_referral_id'	=> array( 
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        ));

		$this->dbforge->add_column('accounts', $fields);

		$this->db->query('ALTER TABLE `accounts` ADD INDEX (`account_created_by_internal_user_id`);');

		

		$fields = array(
		                'referral_id'	=> array(
    										'type' => 'INT',
                                             'constraint' => 11, 
                                             'unsigned' => TRUE,
                                             'auto_increment' => TRUE
					                        ),
		                'referral_account_id'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        ),
		                'referral_internal_user_id'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        ),
		                'referral_code'		=> array(
	            									'type'			=> 'VARCHAR',
	            									'constraint'	=> 10,
	            									'null'			=> TRUE
	            									)
		                );
		

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('referral_id', TRUE);

		$this->dbforge->create_table('referrals', TRUE);	

		$this->db->query('ALTER TABLE `referrals` ADD INDEX (`referral_code`);');

		$accounts = $this->model('account')->get_all();

		foreach($accounts as $account)
		{
			$this->model('referral')->insert(array('referral_account_id' => $account->account_id));
		}

		$users = $this->model('internal_user')->get_all();

		foreach($users as $user)
		{
			$this->model('referral')->insert(array('referral_internal_user_id' => $user->internal_user_id));
		}


		// Create the messages table
		$fields = array(
		                'restriction_id'	=> array(
    										'type' => 'INT',
                                             'constraint' => 11, 
                                             'unsigned' => TRUE,
                                             'auto_increment' => TRUE
					                        ),
		                'restriction_account_id'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        ),
		                'restriction_minimum'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 3,
			                                            'default'		=> 0
								                        ),
		                'restriction_maximum'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 3,
			                                            'default'		=> 0
								                        ),
		                'restriction_start_at'	=> array(
		            									'type'			=> 'DATETIME',
		            									'null'			=> FALSE
		            									),
		                'restriction_end_at'		=> array(
		            									'type'			=> 'DATETIME',
		            									'null'			=> FALSE
		            									),
		                
		                'restriction_label'		=> array(
	            									'type'			=> 'VARCHAR',
	            									'constraint'	=> 100,
	            									'null'			=> TRUE
	            									),
		                'restriction_updated_at'	=> array(
            									'type'			=> 'TIMESTAMP',
            									'null'			=> FALSE
            									),
		                'restriction_created_at'	=> array(
            									'type'			=> 'DATETIME',
            									'null'			=> FALSE
            									),
		                'restriction_deleted_at'	=> array(
            									'type'			=> 'DATETIME',
            									'null'			=> FALSE,
            									'default'		=> '0000-00-00 00:00:00'
            									)
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('restriction_id', TRUE);

		$this->dbforge->create_table('restrictions', TRUE);	

		$this->db->query('ALTER TABLE `restrictions` ADD INDEX (`restriction_account_id`);');

		$fields = array(
						'rtr_resource_id'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        ),
						'rtr_restriction_id'	=> array(
    													'type' 			=> 'INT',
			                                            'constraint' 	=> 11,
			                                            'default'		=> 0
								                        )
						);

		$this->dbforge->add_field($fields);

		$this->dbforge->create_table('restriction_to_resource', TRUE);	



		$fields = array('resource_tax_amount' => array(
													'type' => 'FLOAT',
													'default'		=> 0
													),
						'resource_tax_inclusive'	=> array(
															'type' => 'TINYINT',
															'constraint'	=> 1,
															'default'		=> 1
															));

		$this->dbforge->add_column('resources', $fields);

		$fields = array('supplement_tax_amount' => array(
													'type' => 'FLOAT',
													'default'		=> 0
													),
						'supplement_tax_inclusive'	=> array(
															'type' => 'TINYINT',
															'constraint'	=> 1,
															'default'		=> 1
															));

		$this->dbforge->add_column('supplements', $fields);


		$fields = array('booking_price_pre_tax' => array(
													'type' => 'FLOAT',
													'default'		=> 0
													));

		$this->dbforge->add_column('bookings', $fields);


		$fields = array('stb_price_pre_tax' => array(
													'type' => 'FLOAT',
													'default'		=> 0
													));

		$this->dbforge->add_column('supplement_to_booking', $fields);

		$fields = array('reservation_price_pre_tax' => array(
													'type' => 'FLOAT',
													'default'		=> 0
													));

		$this->dbforge->add_column('reservations', $fields);

		$reservations = $this->model('reservation')->get_all();

		foreach($reservations as $reservation)
		{
			$this->db->where('reservation_booking_id', $reservation->reservation_booking_id)
					->where('reservation_resource_id', $reservation->reservation_resource_id)
					->update('reservations', array(
													'reservation_price_pre_tax' => $reservation->reservation_price
													));
		}


		

		$fields = array('internal_user_is_agent' => array(
    													'type' 			=> 'TINYINT',
			                                            'constraint' 	=> 1,
			                                            'default'		=> 0
								                        ));

		$this->dbforge->add_column('internal_users', $fields);

		// Housekeeping


		foreach(array('account', 'booking', 'customer', 'internal_user', 'resource', 'user', 'supplement') as $table)
		{
			$this->db->query("ALTER TABLE `{$table}s` CHANGE `{$table}_deleted_at` `{$table}_deleted_at` DATETIME  NOT NULL  DEFAULT '0000-00-00 00:00:00';");
		}

		$this->db->query("ALTER TABLE `users` CHANGE `user_password_reset_expires` `user_password_reset_expires` DATETIME  NOT NULL  DEFAULT '0000-00-00 00:00:00';");

		$this->db->query("ALTER TABLE `accounts` CHANGE `account_activated_at` `account_activated_at` DATETIME  NOT NULL  DEFAULT '0000-00-00 00:00:00';");

		$this->db->query("ALTER TABLE `bookings` CHANGE `booking_confirmation_sent_at` `booking_confirmation_sent_at` DATETIME  NOT NULL  DEFAULT '0000-00-00 00:00:00';");

		$this->db->query("ALTER TABLE `bookings` CHANGE `booking_session_id` `booking_session_id` VARCHAR(40)  CHARACTER SET utf8  COLLATE utf8_unicode_ci  NULL  DEFAULT '0';
");

	
		/*if(ENVIRONMENT == 'development')
		{
			$this->load->library('PasswordHash', array(8, FALSE));

			$this->model('user')->update(237, array(
													'user_email'	=> 'phil@othertribe.com',
													'user_password' => $this->passwordhash->HashPassword('pcr34t366')));
		}*/

	}

	public function down()
	{
		
	}

	protected function model($name)
	{
		$name = $name . MODEL_SUFFIX;
		
		// is there a module involved
		$model_name = explode('/', $name);
		
		if ( ! isset($this->{end($model_name)}) )
		{
			$this->load->model($name, '', TRUE);
		}

		return $this->{end($model_name)};
	}

}