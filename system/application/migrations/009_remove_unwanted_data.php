<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

class Migration_remove_unwanted_data extends CI_Migration
{
	public function up()
	{
		if(ENVIRONMENT == 'demo')
		{
			// Truncate all the things
			$tables = array('accounts',
						'bookings',
						'customers',
						'logins',
						'prices',
						'referrals',
						'releases',
						'reservations',
						'resources',
						'restriction_to_resource',
						'restrictions',
						'seasons',
						'sessions',
						'settings',
						'supplement_to_booking',
						'supplement_to_resource',
						'supplements',
						'users');
		
			foreach($tables as $table)
			{
				$this->db->truncate($table);
			}
		} else
		{
			// Remove all traces of Reiver's Rest...
			$account = $this->model('account')->get_by('account_slug', 'the-reivers-rest');

			// Use hard deletes...
			// Users
			$this->db->where('user_account_id', $account->account_id)
						->delete('users');

			// Supplements - get all to clear supp to resource
			//$supplements = $this->model('supplement')->get_many_by('supplement_account_id', $account->account_id);

			$supplements = $this->db->where('supplement_account_id', $account->account_id)
									->get('supplements')
									->result();

		
			foreach($supplements as $supplement)
			{
			
				$this->db->where('stb_supplement_id', $supplement->supplement_id)->delete('supplement_to_booking');
				$this->db->where('str_supplement_id', $supplement->supplement_id)->delete('supplement_to_resource');
			}

			$this->db->where('supplement_account_id', $account->account_id)->delete('supplements');

			// Resources
			$resources = $this->db->where('resource_account_id', $account->account_id)
									->get('resources')
									->result();

			foreach($resources as $resource)
			{
				$this->db->where('reservation_resource_id', $resource->resource_id)->delete('reservations');
				$this->db->where('rtr_resource_id', $resource->resource_id)->delete('restriction_to_resource');
				$this->db->where('release_resource_id', $resource->resource_id)->delete('releases');
				$this->db->where('price_resource_id', $resource->resource_id)->delete('prices');
			}

			$this->db->where('resource_account_id', $account->account_id)->delete('resources');


			// Settings
			$this->db->where('setting_account_id', $account->account_id)->delete('settings');

			// Seasons
			$this->db->where('season_account_id', $account->account_id)->delete('seasons');

			// Restrictions
			$this->db->where('restriction_account_id', $account->account_id)->delete('restrictions');
			
			// Referrals
			$this->db->where('referral_account_id', $account->account_id)->delete('referrals');
			// Customers
			$this->db->where('customer_account_id', $account->account_id)->delete('customers');
			// Bookings
			$this->db->where('booking_account_id', $account->account_id)->delete('bookings');

			$this->db->where('account_id', $account->account_id)->delete('accounts');
		}


		// Add some default values...
		$this->db->query("ALTER TABLE `bookings` CHANGE `booking_user_agent` `booking_user_agent` VARCHAR(200) NULL;");

	}

	public function down()
	{
		
	}

	protected function model($name)
	{
		$name = $name . MODEL_SUFFIX;
		
		// is there a module involved
		$model_name = explode('/', $name);
		
		if ( ! isset($this->{end($model_name)}) )
		{
			$this->load->model($name, '', TRUE);
		}

		return $this->{end($model_name)};
	}

}