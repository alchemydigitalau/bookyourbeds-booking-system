<?php 
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Referral_m extends MY_Model
{
	protected $before_create = array('generate_code');

	public function __construct()
	{
		parent::__construct();
	}
	
	public function generate_code($data)
	{
		$this->load->helper('string');

		$data['referral_code'] = strtoupper(random_string('alpha', 10));

		return $data;
	}

	public function check_exists($code)
	{
		$referral = $this->get_by('referral_code', $code);

		return ! empty($referral);
	}

	public function get_referrer($id)
	{
		if ( ! empty($id))
		{
			$referral = $this->get($id);

			if($referral->referral_account_id)
			{
				return $this->db->join('referrals', 'referral_account_id = account_id')
								->where('account_id', $referral->referral_account_id)
								->get('accounts')
								->row();

			} else if($referral->referral_internal_user_id)
			{
				return $this->db->join('referrals', 'referral_internal_user_id = internal_user_id')
								->where('internal_user_id', $referral->referral_internal_user_id)
								->get('internal_users')
								->row();
			}	
		}

		return array();
	}
}