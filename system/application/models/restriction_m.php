<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

class Restriction_m extends MY_Model
{
	protected $before_create = array('extract_resources', 'human_to_mysql');
	protected $before_update = array('extract_resources', 'human_to_mysql');

	protected $after_create = array('do_resources');
	protected $after_update = array('do_resources');

	protected $resources;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_all($account_id = null)
	{
		$this->is_paranoid();

		if( ! empty($account_id))
		{
			$this->db->where('restriction_account_id', $account_id);
		}

		return $this->db->order_by('restriction_start_at', 'desc')
						->get('restrictions')
						->result();
	}

	public function get($id)
	{
		$this->is_paranoid();

		$restriction = $this->db->where('restriction_id', $id)
								->get('restrictions')
								->row();

		$restriction->resources = array();

		if( ! empty($restriction))
		{
			
			$resources = $this->db->where('rtr_restriction_id', $id)
												->get('restriction_to_resource')
												->result();

			foreach($resources as $resource)
			{
				$restriction->resources[] = $resource->rtr_resource_id;
			}
		}

		return $restriction;
	}


	public function get_resource($id)
	{
		//$this->is_paranoid();

		return $this->db->join('restriction_to_resource', 'rtr_restriction_id = restriction_id')
						->where('rtr_resource_id', $id)
						->order_by('restriction_start_at', 'desc')
						->get('restrictions')
						->result();
	}

	public function remove($id, $account_id)
	{
		$restriction = $this->db->where('restriction_id', $id)
								->where('restriction_account_id', $account_id)
								->get('restrictions')
								->row();

		if( ! empty($restriction))
		{
			$this->delete($id);

			$this->db->where('rtr_restriction_id', $id)
						->delete('restriction_to_resource');
		}
	}

	public function human_to_mysql($data)
	{
		if( ! empty($data['restriction_start_at']))
		{
			$data['restriction_start_at'] = human_to_mysql($data['restriction_start_at']);
		}

		if( ! empty($data['restriction_end_at']))
		{
			$data['restriction_end_at'] = human_to_mysql($data['restriction_end_at'], '23:59:59');
		}
		
		return $data;
	}

	public function extract_resources($data)
	{
		if( ! empty($data['resources']))
		{
			$this->resources = $data['resources'];

			unset($data['resources']);
		}

		return $data;
	}

	public function do_resources($data, $id)
	{
		$this->db->where('rtr_restriction_id', $id)
				->delete('restriction_to_resource');

		if( ! empty($this->resources))
		{
			foreach($this->resources as $resource)
			{
				$this->db->insert('restriction_to_resource', array(
																	'rtr_restriction_id' => $id,
																	'rtr_resource_id'	=> $resource
																	));
			}
		}
	}

}