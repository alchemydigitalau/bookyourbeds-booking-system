<?php 
/*
All application code, styles and layouts
Copyright 2013 Phil Stephens
All rights reserved
phil@othertribe.com for more information
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Internal_user_m extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$this->is_paranoid();

		return $this->db->select('internal_users.*')
						->join('referrals', 'referral_internal_user_id = internal_user_id')
						->select('(SELECT COUNT(account_id) FROM accounts WHERE account_referral_id = referral_id AND account_deleted_at = 0) as referrals')
						->get($this->_table)
						->result();
	}

	public function get_referrals($user_id)
	{

		$user = $this->get($user_id);

		//$this->is_paranoid();

		return $this->db->select('accounts.*')
						->where('account_referral_id', $user->referral_id)
						->select('(SELECT SUM(resource_default_release * resource_booking_footprint) FROM resources WHERE resource_account_id = account_id AND resource_active = 1) as account_capacity')
						->select('(SELECT login_at FROM logins JOIN users ON user_id = login_user_id WHERE user_account_id = account_id ORDER BY login_at DESC LIMIT 1) as account_last_activity')
						->where('account_deleted_at', 0)
						->get('accounts')
						->result();
	}

	public function get($primary_value)
	{
		$this->is_paranoid();

		$this->db->select('internal_users.*, referrals.*')
				->join('referrals', 'referral_internal_user_id = internal_user_id')
				->select('(SELECT COUNT(account_id) FROM accounts WHERE account_referral_id = referral_id AND account_deleted_at = 0) as referrals');

		return $this->db->where($this->primary_key, $primary_value)
			->get($this->_table)
			->row();
	}
	
	public function do_signin($username, $password)
	{
		$this->load->library('PasswordHash', array(8, FALSE));

		$this->is_paranoid();
		
		$user = $this->db->where('internal_user_username', $username)
						->join('referrals', 'referral_internal_user_id = internal_user_id')
						->get('internal_users')
						->row();


		if( ! empty($user) && $this->passwordhash->CheckPassword($password, $user->internal_user_password))
		{
			$this->session->set_userdata('internal_user', $user);
			
			return TRUE;
		}

		return FALSE;
	}

	public function check_unique($element, $attempt, $internal_user_id = null)
	{
		if( ! empty($internal_user_id))
		{
			$this->db->where('internal_user_id !=', $internal_user_id);
		}

		$exists = $this->db->where('internal_user_' . $element, $attempt)
						->count_all_results('internal_users');

		return ($exists > 0) ? FALSE : TRUE;
	}

}